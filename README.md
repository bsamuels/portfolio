This repository contains some examples of projects I've worked on, in a variety of languages.
The most common languages you'll find here are C# and C++.
I plan to be adding to it over time, so check back every now and then.

Feel free to take a look at any of the projects.