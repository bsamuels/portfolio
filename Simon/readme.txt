This program was written in C# and XAML using Visual Studio 2012.

It is a WPF application of a "Simon" game, in which a pattern of lights is 
displayed, which the player must then replicate.  It features both 'classic' 
mode and a 'challenge' mode.

More info can be found by clicking the 'Help' button in the app.

