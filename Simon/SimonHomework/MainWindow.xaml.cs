﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/*
 * DONE: Score somewhere on window 
 * DONE: 4 different colored buttons
 * DONE: Start button
 * DONE: Get the game to work as Simon should work
 * DONE: Figure out pattern generation
 * DONE: Display the input timer
 * DONE: Pattern display happens all at once (more than one button is flashed at a time)
 */
namespace SimonHomework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string helpText = "Classic Mode: \r\n" +
            "A button sequence will be displayed, one button at a time. " +
            "Once the timer starts, repeat the displayed pattern to move on to the next " +
            "round. \r\n\r\n" +
            "Challenge Mode:\r\n" +
            "Rules are the same as Classic Mode, but instead of displaying the entire sequence, only the last button is shown. " +
            "It's up to you to remember everything that comes before it.";

        int score = 0;
        bool startGame = false;
        public bool challengeMode = false;
        ButtonSequence gameSequence = new ButtonSequence();
        public InputSequence playerSequence = new InputSequence();
        Timer startTimer = new Timer(50); //Delay between clicking Start and running PlayGame()

        public MainWindow()
        {
            InitializeComponent();
            startTimer.Elapsed += startTimer_Elapsed;
        }

        void startTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            startTimer.Stop();
            Application.Current.Dispatcher.Invoke(GameGenerate);
        }

        private void Start_Click(object sender, RoutedEventArgs e) //Event handler for Start Classic Mode button click
        {
            startGame = true;
            //disable to start & help buttons
            Start.IsEnabled = false;
            StartChallenge.IsEnabled = false;
            Help.IsEnabled = false;

            challengeMode = false;
            Lose.Visibility = System.Windows.Visibility.Hidden; //Hide the lose text, if it is currently visible
            gameSequence.Pattern.Clear(); //Reset the pattern to be matched
            startTimer.Start(); //Make sure Start_Click has ended, then begin the gameplay
        }

        private void Help_Click(object sender, RoutedEventArgs e) //Event handler for the Help button click
        {
            MessageBox.Show(helpText, "Help");
        }

        private void StartChallenge_Click(object sender, RoutedEventArgs e) //Event handler for Start Challenge Mode button click
        {
            startGame = true;
            //disable to start & help buttons
            Start.IsEnabled = false;
            StartChallenge.IsEnabled = false;
            Help.IsEnabled = false;

            challengeMode = true;
            Lose.Visibility = System.Windows.Visibility.Hidden; //Hide the lose text, if it is currently visible
            gameSequence.Pattern.Clear(); //Reset the pattern to be matched
            startTimer.Start(); //Make sure Start_Click has ended, then begin the gameplay
        }
        
        private void Red_Click(object sender, RoutedEventArgs e) //Event handler for Red button click
        {
            if ((startGame == true) && (playerSequence.PlayerTurn == true)) //Only do something if game is started and a sequence isn't being generated
            {
                playerSequence.gameTimer.Stop();
                playerSequence.UserInput.Add(1);
                playerSequence.Time = 0f; //Reset the timer when a button is pressed
                playerSequence.gameTimer.Start();
            }
        }

        private void Blue_Click(object sender, RoutedEventArgs e) //Event handler for Blue button click
        {
            if ((startGame == true) && (playerSequence.PlayerTurn == true)) //Only do something if game is started and a sequence isn't being generated
            {
                playerSequence.gameTimer.Stop();
                playerSequence.UserInput.Add(2);
                playerSequence.Time = 0f; //Reset the timer when a button is pressed
                playerSequence.gameTimer.Start();
            }
        }

        private void Green_Click(object sender, RoutedEventArgs e)//Event handler for Green button click
        {
            if ((startGame == true) && (playerSequence.PlayerTurn == true)) //Only do something if game is started and a sequence isn't being generated
            {
                playerSequence.gameTimer.Stop();
                playerSequence.UserInput.Add(3);
                playerSequence.Time = 0f; //Reset the timer when a button is pressed
                playerSequence.gameTimer.Start();
            }
        }

        private void Yellow_Click(object sender, RoutedEventArgs e) //Event handler for Yellow button click
        {
            if ((startGame == true) && (playerSequence.PlayerTurn == true)) //Only do something if game is started and a sequence isn't being generated
            {
                playerSequence.gameTimer.Stop();
                playerSequence.UserInput.Add(4);
                playerSequence.Time = 0f; //Reset the timer when a button is pressed
                playerSequence.gameTimer.Start();
            }
        }

        /// <summary>
        /// Generate sequence
        /// </summary>
        private void GameGenerate()
        {
            score = gameSequence.Pattern.Count; //Add to player's score for each round won
            scoreText.Text = score.ToString();

            //Generate and display the buttons to the user must press
            gameSequence.Pattern = gameSequence.CreateSequence();

            playerSequence.UserInput.Clear(); //Reset the user input
            playerSequence.Time = 0f;
        }

        /// <summary>
        /// Compare sequences and lose the game, if necessary
        /// </summary>
        public void GameCheck()
        {
            if (playerSequence.Time > playerSequence.roundTime) //Make sure the timer doesn't display some really small, less-than-0 number
            {
                timer.Text = "-";
            }

            playerSequence.UserInput = playerSequence.ReturnInput();

            //Check if the correct buttons were pressed after disabling user input
            gameSequence.GameLost = gameSequence.CheckSequence(playerSequence.UserInput, gameSequence.Pattern);

            if (gameSequence.GameLost == true) //Sequence was entered wrong, so the game is lost
            {
                Lose.Visibility = System.Windows.Visibility.Visible; //Display the lose text when the game is lost

                startGame = false; //Allow the user to start a new game
                Start.IsEnabled = true;
                StartChallenge.IsEnabled = true;
                Help.IsEnabled = true;
                gameSequence.GameLost = false;
            }
            else //Sequence was entered correctly, so begin the next round
            {
                GameGenerate();
            }
        }
    }
}
