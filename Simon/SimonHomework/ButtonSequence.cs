﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace SimonHomework
{
    class ButtonSequence
    {
        public bool GameLost { get; set; }
        private List<int> _pattern; //The pattern of colors that must be pressed

        public List<int> Pattern
        {
            get { return _pattern; }
            set { _pattern = value; }
        }

        private int patternIndex = 0;

        Random rand = new Random();
        Timer wait = new Timer(500); //How long the button to be pressed will be displayed
        Timer flashDelay = new Timer(80); //Delay between button flashes

        public ButtonSequence()
        {
           _pattern = new List<int>();
            GameLost = false;
            wait.Elapsed += wait_Elapsed;
            flashDelay.Elapsed += flashDelay_Elapsed;
        }

        void flashDelay_Elapsed(object sender, ElapsedEventArgs e)
        {
            patternIndex++;
            buttonFlash(_pattern[patternIndex]);
            flashDelay.Stop();
        }

        void wait_Elapsed(object sender, ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
                {
                    var window = ((MainWindow)Application.Current.MainWindow); //Allows things in MainWindow to be called in ButtonSequence
                    window.Red.Opacity = 1f;
                    window.Blue.Opacity = 1f;
                    window.Green.Opacity = 1f;
                    window.Yellow.Opacity = 1f;

                    if (patternIndex < _pattern.Count - 1)
                    {
                        flashDelay.Start();
                    }
                    else //Don't start the player's turn until all buttons have been flashed
                    {
                        window.playerSequence.gameTimer.Start();
                        window.playerSequence.PlayerTurn = true;
                    }
                });
            wait.Stop();
        }

        /// <summary>
        /// Adds and displays the sequence for the user to copy
        /// </summary>
        public List<int> CreateSequence()
        {
            var window = ((MainWindow)Application.Current.MainWindow); //Allows things in MainWindow to be called in ButtonSequence
            _pattern.Add(rand.Next(4) + 1);

            if (window.challengeMode == false)
            {
                patternIndex = 0;
                buttonFlash(_pattern[patternIndex]);
            }
            else
            {
                patternIndex = (_pattern.Count - 1);
                buttonFlash(_pattern[patternIndex]);
            }

            return _pattern;
        }

        /// <summary>
        /// Checks if the user's input matches the created sequence
        /// </summary>
        /// <param name="UserSequence">The sequence clicked by the user</param>
        /// <param name="SequenceToMatch">The sequence generated by the computer</param>
        /// <returns>False if sequences match, True if sequences don't match</returns>
        public bool CheckSequence(List<int> UserSequence, List<int> SequenceToMatch)
        {
            // If sequence counts aren't equal, then we already know the game is lost
            // There is no need to compare each index of the User and ToMatch sequences
            if (UserSequence.Count == SequenceToMatch.Count)
            {
                for (int i = 0; i < SequenceToMatch.Count; i++)
                {   //Check if the user's selection matches the computer sequence
                    if ((UserSequence[i] == SequenceToMatch[i]))
                    {
                        GameLost = false;
                    }
                    else
                    {
                        GameLost = true;
                    }
                }
            }
            else { GameLost = true; }

            return GameLost;
        }

        public void buttonFlash(int color)
        {
            Application.Current.Dispatcher.Invoke(() =>
                {
                    var window = ((MainWindow)Application.Current.MainWindow); //Allows things in MainWindow to be called in ButtonSequence
                    wait.Elapsed += new ElapsedEventHandler(wait_Elapsed);
                    if (color == 1) //Red
                    {
                        window.Red.Opacity = 0.5f;
                        wait.Start();
                    }
                    else if (color == 2) //Blue
                    {
                        window.Blue.Opacity = 0.5f;
                        wait.Start();
                    }
                    else if (color == 3) //Green
                    {
                        window.Green.Opacity = 0.5f;
                        wait.Start();
                    }
                    else //Yellow
                    {
                        window.Yellow.Opacity = 0.5f;
                        wait.Start();
                    }
                });
        }
    }
}
