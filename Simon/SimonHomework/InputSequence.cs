﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace SimonHomework
{
    public class InputSequence : INotifyPropertyChanged
    {
        public bool PlayerTurn { get; set; } //Check if the sequence is being generated, or if the user can input buttons
        private List<int> _userInput; //Track the buttons pressed by the user
        public int roundTime = 2;

        public List<int> UserInput
        {
            get { return _userInput; }
            set { _userInput = value; }
        }

        private float _time;

        public float Time
        {
            get { return _time; }
            set { 
                _time = value;
                var changed = PropertyChanged;
                if (changed != null)
                {
                    changed(this, new PropertyChangedEventArgs("Time"));
                }
            }
        }
        

        public Timer gameTimer = new Timer(100); //How long the user has to press the next button

        public InputSequence()
        {
            _userInput = new List<int>();
            gameTimer.Elapsed += gameTimer_Elapsed;
        }

        void gameTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var t = sender as Timer;
            _time += (float)t.Interval / 1000f;
            Application.Current.Dispatcher.Invoke(() =>
                {
                    var window = ((MainWindow)Application.Current.MainWindow); //Allows things in MainWindow to be called in InputSequence
                    window.timer.Text = (roundTime - _time).ToString(); //Update the game timer
                    if (_time >= roundTime) //The time to hit the next button is up, so check the sequence
                    {
                        window.GameCheck();
                    }
                });
        }

        /// <summary>
        /// Returns the user's input sequence
        /// </summary>
        public List<int> ReturnInput()
        {
            PlayerTurn = false;
            gameTimer.Stop();
            return _userInput;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
