This program was written in C++ using Visual Studio 2013.

It features a deck class that creates a vector of a standard 52 card deck, 
along with the ability to shuffle and draw the top card from the deck.

It also features a class containing the logic to play a game of Blackjack.
The player is dealt two cards, and may then decide whether or not to get another card.
The dealer is also dealt two cards, and will continue to get a card as long as its score is less than 17.