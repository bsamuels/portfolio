#pragma once
#include <string>
#include <exception>

class NoCardsException : public std::exception
{
private:
	std::string error;
	NoCardsException();

public:
	NoCardsException(std::string errorIn)
		: error(errorIn){}
	std::string what() { return error; }
};