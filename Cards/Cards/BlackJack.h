#pragma once
#include "Deck.h"
#include <vector>
#include <string>
class BlackJack
{
public:
	BlackJack(Deck* deckIn);
	~BlackJack();

private:
	/** Constructor taking in a Deck* should be used over default constructor */
	BlackJack();

	/** How many cards the player will start with */
	const int NUM_START_CARDS = 2;

	/** Deck being used to play the game */
	Deck* gameDeck;

	/** Player's hand */
	std::vector<Deck::Card> playerHand;

	/** Dealer's hand */
	std::vector<Deck::Card> dealerHand;

	std::string playerInput;

	int playerScore;

	int dealerScore;

	/** Displays the hand and score of the input */
	void DisplayHandInfo(const std::vector<Deck::Card> handIn, int& scoreIn);

public:
	/** Performs initial game tasks (deal first cards, etc.) */
	void Start();

	/** Handles gameplay loop */
	void Play();

	/** Displays results after Play() ends */
	void DisplayResults();
};

