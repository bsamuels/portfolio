// Program.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Deck.h"
#include "BlackJack.h"
#include <iostream>

Deck* myDeck;
BlackJack game(myDeck);

int _tmain(int argc, _TCHAR* argv[])
{
	game.Start();
	game.Play();
	game.DisplayResults();

	// Keep console window open
	std::cin.clear();
	fflush(stdin);

	std::cout << "Press 'Enter' to exit...";
	std::getchar();

	return 0;
}

