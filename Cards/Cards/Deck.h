#pragma once
#include <ostream>
enum ECardSuit
{
	Club,
	Diamond,
	Heart,
	Spade
};

enum ECardRank
{
	Ace = 1,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King
};

class Deck
{
public:
	Deck();
	~Deck();

	/** Struct to contain card rank and card suit */
	struct Card
	{
		ECardRank rank;
		ECardSuit suit;
	};

private:
	const static int DECK_SIZE = 52;

	/** How many cards have been dealt (used for getting top card) */
	int cardsDealt;

	/** Array of Cards to serve as deck */
	Card cardDeck[DECK_SIZE];

	/** Swap position of two cards in the deck */
	void SwapCards(Card& cardA, Card& cardB);

public:
	/** Convert card data from enums to string */
	static std::string CardToString(const Card& cardIn);

	/** Shuffle the deck (perform certain amount of random swaps) */
	void Shuffle(int numSwaps=5);

	/** Return first card in deck */
	Card GetTopCard();
	
	/** Overload operator<< */
	friend std::ostream& operator<<(std::ostream& os, const Card& cardIn);
};

