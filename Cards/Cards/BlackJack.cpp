#include "stdafx.h"
#include "BlackJack.h"
#include "NoCardsException.h"
#include <iostream>


BlackJack::BlackJack()
{
	// Create default deck, allocate memory for it, and set gameDeck pointer to that memory
	Deck* defaultDeck = new Deck;
	gameDeck = defaultDeck;
	playerScore = 0;
	dealerScore = 0;
}

BlackJack::BlackJack(Deck* deckIn)
{
	// Allocate memory for deck and set gameDeck pointer to that memory
	deckIn = new Deck;
	gameDeck = deckIn;
	playerScore = 0;
	dealerScore = 0;
}


BlackJack::~BlackJack()
{
	// De-allocate memory for gameDeck
	delete gameDeck;
	gameDeck = nullptr;
}

void BlackJack::Start()
{
	std::cout << "Welcome to Blackjack!" << std::endl;
	playerScore = 0;
	dealerScore = 0;
	// playerHand and dealerHand should be empty to start with
	playerHand.clear(); 
	dealerHand.clear();

	gameDeck->Shuffle(30);

	// Deal player starting cards
	for (int i = 0; i < NUM_START_CARDS; i++)
	{
		playerHand.push_back(gameDeck->GetTopCard());
		dealerHand.push_back(gameDeck->GetTopCard());
	}
}

void BlackJack::Play()
{
	// Exit loop when playerScore is at least 21, or when player doesn't want another card
	while ((playerScore < 21) && (playerInput != "no"))
	{
		// Display player's current hand and score
		DisplayHandInfo(playerHand, playerScore);
		// Don't ask player if they want another card if they've just won or lost
		if (playerScore >= 21)
			break;

		// Handle dealer behavior
		if (dealerScore < 18)
		{
			try
			{
				dealerHand.push_back(gameDeck->GetTopCard());
			}
			catch (NoCardsException e) // Do nothing for dealer if there are no more cards
			{}
			// Need to calculate dealer's score, but not display it
			dealerScore = 0;
			for (auto card : dealerHand)
			{
				if (card.rank <= 10){
					dealerScore += card.rank;
				}
				else{
					dealerScore += 10;
				}
			}
		}

		// Ask if player wants another card
		// Bad input (neither 'yes' nor 'no') will result in being asked again
		do
		{
			std::cout << "\nWould you like another card? 'yes' \\ 'no' ";
			std::cin >> playerInput;
			std::cout << " " << std::endl;
		} while ((playerInput != "yes") && (playerInput != "no"));

		// If they do, deal them another card
		if (playerInput == "yes")
		{
			try
			{
				playerHand.push_back(gameDeck->GetTopCard());
			}
			catch (NoCardsException e)
			{
				std::cout << e.what() << std::endl;
			}
		}
	}
}

void BlackJack::DisplayResults()
{
	std::cout << "\nResults: " << std::endl;
	std::cout << "You: ";
	DisplayHandInfo(playerHand, playerScore);

	std::cout << "\nDealer: ";
	DisplayHandInfo(dealerHand, dealerScore);

	if ((playerScore <= 21) && ((playerScore > dealerScore) || (dealerScore > 21)))
	{
		std::cout << "You win!" << std::endl;
	}
	else if ((playerScore > 21) || ((playerScore <= dealerScore) && (dealerScore <= 21)))
	{
		std::cout << "You lose!" << std::endl;
	}
}

void BlackJack::DisplayHandInfo(const std::vector<Deck::Card> handIn, int& scoreIn)
{
	// All cards will be added to the score, not just new ones. Need to make sure old scores aren't counted twice
	scoreIn = 0;

	std::cout << "Hand: " << std::endl;
	for (auto card : handIn)
	{
		std::cout << card << ";\t";
		if (card.rank <= 10){
			scoreIn += card.rank;
		}
		else{
			scoreIn += 10;
		}
	}
	std::cout << "\n\nScore: " << scoreIn << std::endl;
}