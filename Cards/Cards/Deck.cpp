#include "stdafx.h"
#include "Deck.h"
#include "NoCardsException.h"
#include <time.h>
#include <random>


Deck::Deck()
{
	// Seed random value
	srand(time(NULL));

	// No cards have been dealt when the Deck is created
	cardsDealt = 0;

	// Initialize cardDeck so there is one of each card
	for (int i = 0; i < DECK_SIZE; i++)
	{
		cardDeck[i].rank = static_cast<ECardRank>((i % 13) + 1);
		cardDeck[i].suit = static_cast<ECardSuit>(i % 4);
	}
}


Deck::~Deck()
{
}

void Deck::SwapCards(Card& cardA, Card& cardB)
{
	Card tempCard = cardA; // Create tempCard to store data of cardA
	cardA = cardB; // Set cardA data to cardB data
	cardB = tempCard; // Set cardB data to cardA data (stored in tempCard)
}

void Deck::Shuffle(int numSwaps)
{
	for (int i = 0; i < numSwaps; i++)
	{
		// Get two cards from random places in deck
		int cardPosA = rand() % DECK_SIZE;
		int cardPosB;

		// Make sure second card position isn't the same as the first
		do
		{
			cardPosB = rand() % DECK_SIZE;
		} while (cardPosB == cardPosA);

		// At this point, card positions won't be the same, so we can perform the swap
		SwapCards(cardDeck[cardPosA], cardDeck[cardPosB]);
	}
}

Deck::Card Deck::GetTopCard()
{
	// Make sure we don't try to deal cards after all cards have been dealt
	if (cardsDealt < DECK_SIZE)
	{
		return cardDeck[cardsDealt++]; // Need to increment cardsDealt after returning top card
	}
	else
	{
		throw NoCardsException("All cards have been dealt!");
	}
}

std::string Deck::CardToString(const Card& cardIn)
{
	std::string rank;
	std::string suit;

	switch (cardIn.rank)
	{
	case ECardRank::Ace:
		rank = "Ace";
		break;
	case ECardRank::Two:
		rank = "2";
		break;
	case ECardRank::Three:
		rank = "3";
		break;
	case ECardRank::Four:
		rank = "4";
		break;
	case ECardRank::Five:
		rank = "5";
		break;
	case ECardRank::Six:
		rank = "6";
		break;
	case ECardRank::Seven:
		rank = "7";
		break;
	case ECardRank::Eight:
		rank = "8";
		break;
	case ECardRank::Nine:
		rank = "9";
		break;
	case ECardRank::Ten:
		rank = "10";
		break;
	case ECardRank::Jack:
		rank = "Jack";
		break;
	case ECardRank::Queen:
		rank = "Queen";
		break;
	case ECardRank::King:
		rank = "King";
		break;
	}

	switch (cardIn.suit)
	{
	case ECardSuit::Club:
		suit = "Clubs";
		break;
	case ECardSuit::Diamond:
		suit = "Diamonds";
		break;
	case ECardSuit::Heart:
		suit = "Hearts";
		break;
	case ECardSuit::Spade:
		suit = "Spades";
		break;
	}
	return (rank + " of " + suit);
}

// Overload output operator to accept card type
std::ostream& operator<<(std::ostream& os, const Deck::Card& cardIn)
{
	os << Deck::CardToString(cardIn);
	return os;
}