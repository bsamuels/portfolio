Ben Samuels - 2016

This software allows the user to create simple compositions of basic sine waves, and save those compositions to a .WAV file.
In its current state, the range is only one octave, starting at Middle C, and all notes have a duration of 1 second.

The "wish list" of features I would like to add, if/ when I have time to extend the project, is:
    * Get user confirmation before clearing out the notes
    * Preview the composition in the editor before saving to a file
    * Allow the user to change the duration of individual notes
    * Allow the user to change the pitch of individual notes

Treble clef image from [commons.wikimedia.org]