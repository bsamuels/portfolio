﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Composer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int INITIAL_OFFSET_X = 110;
        private int noteOffsetX;

        public MainWindow()
        {
            InitializeComponent();
            noteOffsetX = INITIAL_OFFSET_X;
        }

        private void Note_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Cast the sender as a button, and its CommandParameter to a Notes enum
                Button noteButton = (sender as Button);
                Notes noteType = (Notes)noteButton.CommandParameter;

                // Notes will only differ in position, so appearance can be set outside the Switch
                bool drawNote = true; // If we added a rest, we don't want to add the ellipse to the Staff
                Ellipse noteImg = new Ellipse();
                noteImg.Height = 23;
                noteImg.Width = 35;
                noteImg.Stroke = new SolidColorBrush(Colors.Black);
                noteImg.StrokeThickness = 2;
                noteImg.Tag = "OkForClear"; // Set a tag to make clearing all added notes easier

                // Where the note gets drawn will depend on the note we get back from the CommandParameter, and how many notes are already up
                switch (noteType)
                {
                    case Notes.C:
                        // Set note position
                        Canvas.SetLeft(noteImg, noteOffsetX);
                        Canvas.SetTop(noteImg, 141.5);
                        
                        // Set ledger line position/ size info, and add it to the Staff
                        Line ledger = new Line();
                        ledger.X1 = noteOffsetX - 5;
                        ledger.X2 = noteOffsetX + 40;
                        ledger.Y1 = 153;
                        ledger.Y2 = 153;
                        ledger.Tag = "OkForClear";
                        Staff.Children.Add(ledger);
                        break;

                    case Notes.D:
                        // Set note position
                        Canvas.SetLeft(noteImg, noteOffsetX);
                        Canvas.SetTop(noteImg, 130);
                        break;

                    case Notes.E:
                        // Set note position
                        Canvas.SetLeft(noteImg, noteOffsetX);
                        Canvas.SetTop(noteImg, 118.5);
                        break;

                    case Notes.F:
                        // Set note position
                        Canvas.SetLeft(noteImg, noteOffsetX);
                        Canvas.SetTop(noteImg, 107);
                        break;

                    case Notes.G:
                        // Set note position
                        Canvas.SetLeft(noteImg, noteOffsetX);
                        Canvas.SetTop(noteImg, 95.5);
                        break;

                    case Notes.A:
                        // Set note position
                        Canvas.SetLeft(noteImg, noteOffsetX);
                        Canvas.SetTop(noteImg, 84);
                        break;

                    case Notes.B:
                        // Set note position
                        Canvas.SetLeft(noteImg, noteOffsetX);
                        Canvas.SetTop(noteImg, 72.5);
                        break;

                    case Notes.REST:
                        // Rest is indicated by a rectangle, rather than an ellipse.  Create and add it here
                        Rectangle restImg = new Rectangle();
                        restImg.Width = 35;
                        restImg.Height = 11.5;
                        restImg.Stroke = new SolidColorBrush(Colors.Black);
                        restImg.Fill = new SolidColorBrush(Colors.Black);

                        Canvas.SetLeft(restImg, noteOffsetX);
                        Canvas.SetTop(restImg, 61);
                        restImg.Tag = "OkForClear";

                        drawNote = false;
                        Staff.Children.Add(restImg);
                        break;
                    default:
                        break;
                }

                if(drawNote)
                    Staff.Children.Add(noteImg);

                noteOffsetX += 35;
            }

            catch (NullReferenceException ex)
            {
                MessageBox.Show("An error occurred while adding note to the display");
                Console.WriteLine(ex);
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            // Remove all notes (and any ledger lines) from the UI
            for(int i = Staff.Children.Count - 1; i >= 0; --i)
            {
                if ((Staff.Children[i] as FrameworkElement)?.Tag?.ToString() == "OkForClear")
                    Staff.Children.RemoveAt(i);
            }
            noteOffsetX = INITIAL_OFFSET_X;
        }
    }
}
