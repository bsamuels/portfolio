﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composer
{
    class ComposerModel
    {
        // Collection of notes (actually just a collection of frequencies)
        private List<double> composition;
        public List<double> Composition
        {
            get { return composition; }
        }

        // Since WaveHeader contains only constants, no need to create a member variable for that chunk
        // Same goes for WaveFormat
        private WaveData dataChunk; // WaveData is the only chunk that can't be completely pre-determined programmatically

        public ComposerModel()
        {
            composition = new List<double>();
        }

        public void AddNote(Notes noteToAdd)
        {
            switch (noteToAdd)
            {
                case Notes.C:
                    composition.Add(261.626);
                    break;
                case Notes.D:
                    composition.Add(293.665);
                    break;
                case Notes.E:
                    composition.Add(329.628);
                    break;
                case Notes.F:
                    composition.Add(349.228);
                    break;
                case Notes.G:
                    composition.Add(391.995);
                    break;
                case Notes.A:
                    composition.Add(440.000);
                    break;
                case Notes.B:
                    composition.Add(493.883);
                    break;
                case Notes.REST:
                default:
                    composition.Add(000.000);
                    break;
            }
        }

        public void ClearComposition()
        {
            composition.Clear();
        }

        public void SaveToFile(string fileName)
        {
            // First, actually set the data for the WAV
            PopulateSampleData();
            // Then we can create the file in the given location
            FileStream fileStream = null;
            try
            {
                fileStream = new FileStream(fileName, FileMode.Create);
                // 'using' block will handle BinaryWriter closing
                using (BinaryWriter binWriter = new BinaryWriter(fileStream))
                {
                    // Write header chunk
                    binWriter.Write(WaveHeader.sGroupID.ToCharArray());
                    binWriter.Write(WaveHeader.dwFileLength);
                    binWriter.Write(WaveHeader.sRiffType.ToCharArray());

                    // Write format chunk
                    binWriter.Write(WaveFormat.sChunkID.ToCharArray());
                    binWriter.Write(WaveFormat.dwChunkSize);
                    binWriter.Write(WaveFormat.wformatTag);
                    binWriter.Write(WaveFormat.wChannels);
                    binWriter.Write(WaveFormat.dwSamplesPerSec);
                    binWriter.Write(WaveFormat.dwAvgBytesPerSec);
                    binWriter.Write(WaveFormat.wBlockAlign);
                    binWriter.Write(WaveFormat.wBitsPerSample);

                    // Write data chunk
                    binWriter.Write(WaveData.sChunkID.ToCharArray());
                    binWriter.Write(dataChunk.dwChunkSize);
                    for (int i = 0; i < dataChunk.shortArray.Length; ++i)
                        binWriter.Write(dataChunk.shortArray[i]);

                    // Specify the file length, now that we have that data
                    binWriter.Seek(4, SeekOrigin.Begin);
                    uint fileSize = (uint)binWriter.BaseStream.Length;
                    binWriter.Write(fileSize - 8);
                }
            }

            finally
            {
                if (fileStream != null)
                    fileStream.Dispose();
            }
        }

        private void PopulateSampleData()
        {
            // Each note will last one second across all channels, so numSamples = samplesPerSec * numChannels * composition.Count
            dataChunk = new WaveData((int)(WaveFormat.dwSamplesPerSec * WaveFormat.wChannels * composition.Count));

            int amplitude = 32760; // Maximum amplitude for 16-bit audio
            // Store the samples in the dataChunk's array
			// There is some popping between notes, unfortunately fixing that is beyond my current scope for this project
            for(int note = 0; note < composition.Count; ++note)
            {
                double theta = (Math.PI * 2 * composition[note]) / (WaveFormat.dwSamplesPerSec * WaveFormat.wChannels);

                for (uint sample = 0; sample < WaveFormat.dwSamplesPerSec - 1; ++sample)
                    dataChunk.shortArray[(note * WaveFormat.dwSamplesPerSec) + sample] = (short)(amplitude * Math.Sin(theta * sample));

                ///// If multi-channel audio is ever implemented, will need to use the following code instead.  /////
                ///// Since it's just Mono for now, though, the Channel 'for' loop would be unneccesary         /////

                //for (uint sample = 0; sample < WaveFormat.dwSamplesPerSec - 1; ++sample)
                //{
                //    for (int channel = 0; channel < WaveFormat.wChannels; ++channel)
                //        dataChunk.shortArray[(note * WaveFormat.dwSamplesPerSec) + sample + channel] = (short)(amplitude * Math.Sin(theta * composition[note]));
                //}
            }
        }
    }
}
