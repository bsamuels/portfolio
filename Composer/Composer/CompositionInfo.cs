﻿namespace Composer
{
    public enum Notes
    {
        C,
        D,
        E,
        F,
        G,
        A,
        B,
        REST
    }

    struct WaveHeader
    {
        public const string sGroupID = "RIFF"; // Will always be a RIFF format
        public const uint dwFileLength = 0; // Will be determined retro-actively when writing the file. For now, say '0' as a placeholder
        public const string sRiffType = "WAVE"; // Will always be a Wave file
    }

    struct WaveFormat
    {
        public const string sChunkID = "fmt ";
        public const uint dwChunkSize = 16; // Format chunk is always 16 bytes
        public const ushort wformatTag = 1; // PCM Format
        public const ushort wChannels = 1; // Will only be playing in mono
        public const uint dwSamplesPerSec = 44100; // 44.1 kHz. Actually "frames per second"
        public const uint dwAvgBytesPerSec = wBlockAlign * dwSamplesPerSec;
        public const ushort wBlockAlign = wChannels * (wBitsPerSample / 8); // How many bytes are in a single frame (a.k.a., sample across all channels)
        public const ushort wBitsPerSample = 16; // a.k.a., Bit Depth of the audio
    }

    struct WaveData
    {
        public const string sChunkID = "data";
        public readonly uint dwChunkSize;
        public readonly short[] shortArray; // The actual audio data being sampled

        public WaveData(int numSamples)
        {
            shortArray = new short[numSamples];
            dwChunkSize = (uint)(numSamples * (WaveFormat.wBitsPerSample / 8)); // Size of the data chunk = (number of samples) * (size of each sample)
        }
    }
}