﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;

namespace Composer
{
    class ComposerViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Data-Bound Properties & Commands
        private ICommand addNoteCommand;
        public ICommand AddNoteCommand
        {
            get { return addNoteCommand; }
        }

        private ICommand clearCommand;
        public ICommand ClearCommand
        {
            get { return clearCommand; }
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
        }
        #endregion

        private ComposerModel model;

        public ComposerViewModel()
        {
            model = new ComposerModel();

            // Hook up commands
            addNoteCommand = new RelayCommand<Notes>(AddNote);
            clearCommand = new RelayCommand<object>(ClearView);
            saveCommand = new RelayCommand<object>(SaveToWav);
        }

        // NoteAdded (specific note for data-binding, calls overload taking in specific note type)
        private void AddNote(Notes noteAdded)
        {
            model.AddNote(noteAdded);
        }

        // Remove all notes from the display, and clear out the Model's collection of notes
        private void ClearView(object obj)
        {
            //DialogResult confirm = MessageBox.Show("This action cannot be undone. Proceed?", "Confirm Clear", MessageBoxButtons.YesNo);
            //if(confirm == DialogResult.Yes)
            model.ClearComposition();
        }

        // Save the added notes to a .WAV file
        private void SaveToWav(object obj)
        {
            // Set up save dialog
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "WAV files (*.wav)|*.wav";
            saveDlg.DefaultExt = "wav";
            // If the user confirms the dialog, tell the Model to create the save file
            if (saveDlg.ShowDialog() == DialogResult.OK && saveDlg.FileName.Trim() != "")
                model.SaveToFile(saveDlg.FileName.Trim());
        }

        //public void RaisePropertyChanged(string propName)
        //{
        //    if (this.PropertyChanged != null)
        //        this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        //}
    }
}
